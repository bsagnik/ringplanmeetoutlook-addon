/*
 * Copyright (c) Microsoft Corporation. All rights reserved. Licensed under the MIT license.
 * See LICENSE in the project root for license information.
 */

/* global document, Office */

Office.onReady((info) => {
  if (info.host === Office.HostType.Outlook) {
    document.getElementById("app-body").style.display = "flex";
    const ringplan_email = getSettings("ringplan_email");
    const ringplan_apiKey = getSettings("ringplan_apiKey");


    if (ringplan_email != undefined && ringplan_apiKey != undefined) {
      document.getElementById("header").innerHTML = `
      <header id = "header" class="ms-welcome__header ms-bgColor-neutralLighter">
        <img width="90" height="90" src="../../assets/icon.png" alt="Contoso" title="Contoso" />
        <h1 class="ms-font-su">Welcome</h1>
        <button type ="submit" id ="logout">Logout</button>
      </header>
      `;
      document.getElementById("app-body").innerHTML = `
    <main class="ms-welcome__main">
      <p>ringplan_email: ${ringplan_email}</p>
      <p>API Token: ${ringplan_apiKey}</p>
    </main>
    
  `;
      document.getElementById("logout").onclick = logout;
    } else {
      document.getElementById("login-form").style.display = "block";
      document.getElementById("add").onclick = login;
    }
  }
});

function getSettings(settingsname) {
  return Office.context.roamingSettings.get(settingsname);
}

async function login() {
  const email = document.getElementById("email").value;
  const apiKey = document.getElementById("api-token").value;
  await setSettings(email, apiKey);
  document.getElementById("header").innerHTML = `
      <header id = "header" class="ms-welcome__header ms-bgColor-neutralLighter">
        <img width="90" height="90" src="../../assets/icon.png" alt="Contoso" title="Contoso" />
        <h1 class="ms-font-su">Welcome</h1>
        <button type ="submit" id ="logout">Logout</button>
      </header>
      `;
  document.getElementById("app-body").innerHTML = `
    <main class="ms-welcome__main">
      <p>ringplan_email: ${email}</p>
      <p>API Token: ${apiKey}</p>
    </main>
    
  `;
  document.getElementById("logout").onclick = logout;
  await saveSettings();
}

function setSettings(ringplan_email, ringplan_apiKey) {
  return new Promise(function (resolve, reject) {
    Office.context.roamingSettings.set("ringplan_email", ringplan_email);
    Office.context.roamingSettings.set("ringplan_apiKey", ringplan_apiKey);
    resolve(console.log("Credentials Added"));
  });
}

async function logout() {
  await removeSettings();
  await saveSettings();
  Office.context.ui.closeContainer();
}

function removeSettings() {
  return new Promise(function (resolve, reject) {
    Office.context.roamingSettings.remove("ringplan_email");
    Office.context.roamingSettings.remove("ringplan_apiKey");
    resolve(console.log("Removing Credentials"));
  });
}

function saveSettings() {
  return new Promise(function (resolve, reject) {
    Office.context.roamingSettings.saveAsync(function (asyncResult) {
      if (asyncResult.status == Office.AsyncResultStatus.Failed) {
        resolve(console.log("Error: " + asyncResult.error.message));
      }
      resolve(console.log("Task Success"));
    });
  });
}
