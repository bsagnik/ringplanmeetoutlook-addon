import axios from "axios";

let mailboxItem;
let newBody = "";
let event_id = "";
let config = "";

// Office is ready.
Office.onReady(function () {
  mailboxItem = Office.context.mailbox.item;
  config = {
    headers: { Authorization: `${Office.context.roamingSettings.get("ringplan_apiKey")}` },
  };
});

async function insertRingPlanMeeting(event) {
  if (Office.context.roamingSettings.get("ringplan_apiKey") == undefined) {
    Office.context.ui.displayDialogAsync("https://localhost:3000/taskpane.html");
  }
  const title = await getSubject();
  const start = await getStartTimeDate();
  const end = await getEndTimeDate();
  const required_attendees = await getRequiredAttendes();
  const optional_attendees = await getOptionalAttendes();
  var data = {};

  if (title != "") {
    data["title"] = title;
  } else {
    data["title"] = "Create a ringplan meet";
  }
  data["start"] = start;
  data["end"] = end;
  var allAttendees = required_attendees.concat(optional_attendees);
  var attendeeNames = [];
  for (var i = 0; i < allAttendees.length; i++) {
    attendeeNames.push({ email: allAttendees[i].emailAddress });
  }
  data["attendees"] = attendeeNames;
  Office.context.mailbox.item.get;
  createRingPlanMeet(data, event);
}

function createRingPlanMeet(data, event) {
  axios
    .post("https://ssp-backend.ringplan.com/meet/v2/events", data, config)
    .then((response) => {
      newBody = response.data[0].description_formatted;
      event_id = response.data[0].id;
      const title = response.data[0].title;
      mailboxItem.body.getAsync("html", { asyncContext: event }, function (getBodyResult) {
        if (getBodyResult.status === Office.AsyncResultStatus.Succeeded) {
          mailboxItem.loadCustomPropertiesAsync(saveCustomProperties);
          updateBody(getBodyResult.asyncContext, getBodyResult.value, title);
        } else {
          console.error("Failed to get HTML body.");
          getBodyResult.asyncContext.completed({ allowEvent: false });
        }
      });
    })
    .catch((error) => {
      if (error.response.data) {
        Office.context.mailbox.item.notificationMessages.addAsync("Error", {
          type: "errorMessage",
          message: `${error.response.data.detail}`,
        });
      }
    });
}
function getSubject() {
  return new Promise(function (resolve, reject) {
    Office.context.mailbox.item.subject.getAsync(function (result) {
      resolve(result.value);
    });
  });
}

function getStartTimeDate() {
  return new Promise(function (resolve, reject) {
    Office.context.mailbox.item.start.getAsync(function (result) {
      resolve(result.value);
    });
  });
}

function getEndTimeDate() {
  return new Promise(function (resolve, reject) {
    Office.context.mailbox.item.end.getAsync(function (result) {
      resolve(result.value);
    });
  });
}

function getOptionalAttendes() {
  return new Promise(function (resolve, reject) {
    Office.context.mailbox.item.optionalAttendees.getAsync(function (result) {
      resolve(result.value);
    });
  });
}

function getRequiredAttendes() {
  return new Promise(function (resolve, reject) {
    Office.context.mailbox.item.requiredAttendees.getAsync(function (result) {
      resolve(result.value);
    });
  });
}

function saveCustomProperties(asyncResult) {
  const customProps = asyncResult.value;
  customProps.set("event_id", event_id);
  console.log(event_id);
  customProps.saveAsync();
}

// Register the function.
Office.actions.associate("insertRingPlanMeeting", insertRingPlanMeeting);

function updateBody(event, existingBody, title) {
  mailboxItem.body.setAsync(
    existingBody + newBody,
    { asyncContext: event, coercionType: "html" },

    function (setBodyResult) {
      if (setBodyResult.status === Office.AsyncResultStatus.Succeeded) {
        setBodyResult.asyncContext.completed({ allowEvent: true });
      } else {
        console.error("Failed to set HTML body.");
        setBodyResult.asyncContext.completed({ allowEvent: false });
      }
    }
  );

  Office.context.mailbox.item.subject.setAsync(title);
}
