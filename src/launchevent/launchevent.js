let config;
Office.onReady(function () {
  config = {
    headers: { Authorization: `${Office.context.roamingSettings.get("ringplan_apiKey")}` },
  };
});

async function onAppointmentAttendeesChangedHandler(event) {
  const event_id = await loadCustomProperties();
  const required_attendees = await getRequiredAttendes();
  const optional_attendees = await getOptionalAttendes();
  var allAttendees = required_attendees.concat(optional_attendees);
  var attendeeNames = [];
  for (var i = 0; i < allAttendees.length; i++) {
    attendeeNames.push({ email: allAttendees[i].emailAddress });
  }
  var data = {};
  data["attendees"] = attendeeNames;
  updateRingplanMeet(event_id, data);
}

function getOptionalAttendes() {
  return new Promise(function (resolve, reject) {
    Office.context.mailbox.item.optionalAttendees.getAsync(function (result) {
      resolve(result.value);
    });
  });
}

function getRequiredAttendes() {
  return new Promise(function (resolve, reject) {
    Office.context.mailbox.item.requiredAttendees.getAsync(function (result) {
      resolve(result.value);
    });
  });
}

async function onAppointmentTimeChangedHandler(event) {
  const event_id = await loadCustomProperties();
  const start = await getStartTimeDate();
  const end = await getEndTimeDate();
  var data = {};
  data["start"] = start;
  data["end"] = end;
  updateRingplanMeet(event_id, data);
}

function loadCustomProperties() {
  return new Promise(function (resolve, reject) {
    Office.context.mailbox.item.loadCustomPropertiesAsync(function (result) {
      const customProps = result.value;
      resolve(customProps.get("event_id"));
    });
  });
}

function getStartTimeDate() {
  return new Promise(function (resolve, reject) {
    Office.context.mailbox.item.start.getAsync(function (result) {
      resolve(result.value);
    });
  });
}

function getEndTimeDate() {
  return new Promise(function (resolve, reject) {
    Office.context.mailbox.item.end.getAsync(function (result) {
      resolve(result.value);
    });
  });
}

function updateRingplanMeet(event_id, data) {
  axios
    .patch(`https://ssp-backend.ringplan.com/meet/v2/events/${event_id}`, data, config)
    .then((response) => {
      //#TODO remove console before deployment
      console.log("UPDATE EVENT SUCCESS");
    })
    .catch((error) => {
      if (error.response.data) {
        Office.context.mailbox.item.notificationMessages.addAsync("Error", {
          type: "errorMessage",
          message: `${error.response.data.detail}`,
        });
      }
    });
}

// IMPORTANT: To ensure your add-in is supported in the Outlook client on Windows, remember to map the event handler name specified in the manifest's LaunchEvent element to its JavaScript counterpart.
// 1st parameter: FunctionName of LaunchEvent in the manifest; 2nd parameter: Its implementation in this .js file.
if (Office.context.platform === Office.PlatformType.PC || Office.context.platform == null) {
  Office.actions.associate("onAppointmentAttendeesChangedHandler", onAppointmentAttendeesChangedHandler);
  Office.actions.associate("onAppointmentTimeChangedHandler", onAppointmentTimeChangedHandler);
}
